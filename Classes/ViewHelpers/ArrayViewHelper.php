<?php
namespace RZ\Rzmailchimp\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ArrayViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('options', 'string', '', true);
    }

    /**
     * Render function
     *
     * @return string
     */
    public function render()
    {
        $options = $this->arguments['options'];

        // Create array
        $options = explode("\n", $options);

        $optionArr = [];
        foreach ($options as $option) {
            $option = trim($option);

            // Create array
            $option = explode("|", $option);

            $optionArr[] = $option;
        }

        // Rewrite array for better use in Fluid
        $optionsFinal = [];
        foreach ($optionArr as $options) {
            // Title
            $optionSingle['title'] = $options[0];

            // Value
            if ($options[1]) {
                $optionSingle['value'] = $options[1];
            } else {
                unset($optionSingle['value']);
            }

            $optionsFinal[] = $optionSingle;
        }

        return $optionsFinal;
    }
}
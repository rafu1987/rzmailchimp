<?php
namespace RZ\Rzmailchimp\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class FieldnameViewHelper extends AbstractViewHelper
{

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('field', 'string', '', true);
    }

    /**
     * Render function
     *
     * @return string
     */
    public function render()
    {
        $field = $this->arguments['field'];

        $prefix = 'field';

        $fieldname = strtolower($field);
        $fieldname = str_replace([
            'ä',
            'ö',
            'ü',
            'ß',
        ], [
            'ae',
            'oe',
            'ue',
            'ss',
        ], $fieldname);
        $fieldname = str_replace([
            ' ',
            '-',
            '_',
            ',',
            '.',
            ':',
            ';',
            '?',
            '&',
            '$',
            '§',
            '!',
            '%',
            '/',
            '=',
        ], '', $fieldname);
        $fieldname = ucfirst($fieldname);
        $fieldname = $prefix . $fieldname;

        return $fieldname;
    }

}
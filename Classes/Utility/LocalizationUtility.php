<?php
namespace RZ\Rzmailchimp\Utility;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Utility\LocalizationUtility as ExtbaseLocalizationUtility;

/**
 * Class LocalizationUtility
 * @codeCoverageIgnore
 */
class LocalizationUtility
{
    /**
     * Returns the localized label of the LOCAL_LANG key, but prefill extensionName
     *
     * @param string $key The key from the LOCAL_LANG array for which to return the value.
     * @param string $extensionName The name of the extension
     * @param array $arguments the arguments of the extension, being passed over to vsprintf
     * @return string|null
     */
    public static function translate($key, $extensionName = 'rzmailchimp', $arguments = null)
    {
        return ExtbaseLocalizationUtility::translate($key, $extensionName, $arguments);
    }

}
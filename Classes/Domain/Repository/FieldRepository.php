<?php
namespace RZ\Rzmailchimp\Domain\Repository;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * The repository for Field
 */
class FieldRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function findCustom($fields)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);

        $query->matching($query->in('uid', $fields));

        // Sorting like in FlexForm
        $sorting = [];
        foreach ($fields as $uid) {
            foreach ($query->execute() as $field) {
                if ($field->getUid() == $uid) {
                    $sorting[] = $field;
                }
            }
        }

        return $sorting;
    }

}
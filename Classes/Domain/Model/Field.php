<?php
namespace RZ\Rzmailchimp\Domain\Model;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Field
 */
class Field extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * value
     *
     * @var string
     */
    protected $value = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * options
     *
     * @var string
     */
    protected $options = '';

    /**
     * required
     *
     * @var boolean
     */
    protected $required = '';

    /**
     * multiple
     *
     * @var boolean
     */
    protected $multiple = '';

    /**
     * Input value
     *
     * @var string
     */
    protected $inputValue = '';

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the options
     *
     * @return string $options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the options
     *
     * @param string $options
     * @return void
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * Returns the required
     *
     * @return string $required
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Sets the required
     *
     * @param string $required
     * @return void
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }

    /**
     * Returns the multiple
     *
     * @return string $multiple
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Sets the multiple
     *
     * @param string $multiple
     * @return void
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }

    /**
     * Returns the inputValue
     *
     * @return string $inputValue
     */
    public function getInputValue()
    {
        return $this->inputValue;
    }

    /**
     * Sets the inputValue
     *
     * @param string $inputValue
     * @return void
     */
    public function setInputValue($inputValue)
    {
        $this->inputValue = $inputValue;
    }

}
<?php
namespace RZ\Rzmailchimp\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Powermail
 */
class PowermailController extends \In2code\Powermail\Controller\FormController
{

    /**
     * MailChimp
     *
     * @return void
     */
    public function mailchimp(\In2code\Powermail\Domain\Model\Mail $mail)
    {
        // Get MailChimp settings
        $mailchimpOn = $this->settings['main']['mailchimp'];
        $mailchimpApi = $this->settings['main']['mailchimpApiKey'];
        $mailchimpList = $this->settings['main']['mailchimpListId'];
        $mailchimpFormat = $this->settings['main']['mailchimpFormat'];

        // Check if all necessary settings are set
        if ($mailchimpOn && $mailchimpApi && $mailchimpList) {
            // Get answers
            $answers = $mail->getAnswers();

            // Get fields/answers
            $name = [];
            foreach ($answers as $answer) {
                if ($answer->getField()->isSenderName() === true) {
                    $name[] = $answer->getValue();
                }

                if ($answer->getField()->isSenderEmail() === true) {
                    // Get email
                    $email = $answer->getValue();
                }
            }

            // Name modifications
            if (count($name) == 1) {
                if ($mailchimpFormat == 1) {
                    foreach ($name as $n) {
                        $firstLastName = explode(" ", $n);
                    }

                    if ($firstLastName[1]) {
                        $lastName = $firstLastName[1];
                    } else {
                        $lastName = '';
                    }

                    $firstLastNameFinal = [
                        'FNAME' => $firstLastName[0],
                        'LNAME' => $lastName,
                    ];
                }
            } else {
                if ($mailchimpFormat == 2) {
                    $firstLastNameFinal = [
                        'FNAME' => $name[0],
                        'LNAME' => $name[1],
                    ];
                } else if ($mailchimpFormat == 3) {
                    $firstLastNameFinal = [
                        'LNAME' => $name[0],
                        'FNAME' => $name[1],
                    ];
                }
            }

            // MailChimp stuff
            if ($email) {
                // Connect to MailChimp API
                $mailChimp = new \RZ\Rzmailchimp\Api\MailChimp($mailchimpApi);

                // Options
                $finalOptions = [
                    'email_address' => $email,
                    'merge_fields' => (object) $firstLastNameFinal,
                    'status' => 'subscribed',
                ];

                $result = $mailChimp->post('lists/' . $mailchimpList . '/members', $finalOptions);

                // Member already exists
                if ($result['title'] == 'Member Exists') {
                    $subscriberHash = $mailChimp->subscriberHash($email);

                    $result = $mailChimp->put('lists/' . $mailchimpList . '/members/' . $subscriberHash, $finalOptions);
                }
            }
        }
    }

}
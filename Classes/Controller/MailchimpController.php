<?php
namespace RZ\Rzmailchimp\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use FluidTYPO3\Vhs\Asset;
use RZ\Rzmailchimp\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Mailchimp
 */
class MailchimpController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * fieldRepository
     *
     * @var \RZ\Rzmailchimp\Domain\Repository\FieldRepository
     * @inject
     */
    protected $fieldRepository = null;

    /**
     * action display
     *
     * @return void
     */
    public function displayAction()
    {
        // Parsley?
        if ($this->settings['addParsley'] === 1) {
            // Dependencies
            $dependencies = '';
            if (ExtensionManagementUtility::isLoaded('razor')) {
                $dependencies = 'razorVendorJs';
            }

            // Assets
            Asset::createFromSettings([
                'name' => 'rzmailchimpParsley',
                'path' => 'EXT:rzmailchimp/Resources/Public/Js/parsley.min.js',
                'async' => 1,
                'dependencies' => $dependencies,
            ]);
        }

        // Get arguments
        $args = $this->request->getArguments();

        // Fields
        $fieldsSettings = explode(",", $this->settings['fields']);
        $fields = $this->fieldRepository->findCustom($fieldsSettings);

        if ((bool) $args['error'] === true) {
            $this->addFlashMessage(
                LocalizationUtility::translate('errorMessage'),
                '',
                FlashMessage::ERROR
            );

            foreach ($fields as $field) {
                $value = $field->getValue();
                $field->setInputValue($args['fields'][$value]);
            }
        } else if ((bool) $args['success'] === true) {
            $this->addFlashMessage(
                LocalizationUtility::translate('successMessage'),
                '',
                FlashMessage::OK
            );
        }

        // Get content uid
        $this->contentObj = $this->configurationManager->getContentObject();
        $uid = $this->contentObj->data['uid'];

        // Send label
        $sendLabel = $this->settings['sendLabel'];

        $this->view->assign('fields', $fields);
        $this->view->assign('sendLabel', $sendLabel);
        $this->view->assign('uid', $uid);
    }

    /**
     * action subscribe
     *
     * @return void
     */
    public function subscribeAction()
    {
        // Get arguments
        $args = $this->request->getArguments();
        $fields = $args['fields'];

        // Connect to Mailchimp API
        $mailchimp = new \RZ\Rzmailchimp\Api\MailChimp($this->settings['apiKey']);

        // // Get fields from settings
        $fieldsSettings = explode(",", $this->settings['fields']);
        $fieldsAll = $this->fieldRepository->findCustom($fieldsSettings);

        foreach ($fieldsAll as $f) {
            if ($f->getType() == 'Email') {
                $emailValue = $f->getValue();
                break;
            }
        }

        $email = $fields[$emailValue];
        unset($fields[$emailValue]);

        // Status
        if ($this->settings['doubleOptin']) {
            $status = 'pending';
        } else {
            $status = 'subscribed';
        }

        // Get audience id
        $audienceId = (string) $this->settings['audienceId'];

        // Options
        $finalOptions = [
            'email_address' => $email,
            'merge_fields' => (object) $fields,
            'status' => $status,
        ];

        // Interests
        if ($this->settings['interests']) {
            $interestsArr = explode(",", $this->settings['interests']);

            foreach ($interestsArr as $interest) {
                $interestsFinal[trim($interest)] = true;
            }

            $finalOptions['interests'] = (object) $interestsFinal;
        }

        // Subscribe
        $result = $mailchimp->post('lists/' . $audienceId . '/members', $finalOptions);

        // Member already exists
        if ($result['title'] == 'Member Exists' && $this->settings['updateExisting']) {
            $subscriberHash = $mailchimp->subscriberHash($email);

            $result = $mailchimp->put('lists/' . $audienceId . '/members/' . $subscriberHash, $finalOptions);
        }

        if (!$result) {
            // Readd email
            $fields[$emailValue] = $email;

            $arguments = [
                'error' => true,
                'fields' => $fields,
            ];

            $this->redirect('display', 'Mailchimp', 'rzmailchimp', $arguments);
        }

        // Redirect
        if ($this->settings['redirectPid']) {
            $redirectUrl = $this->controllerContext
                ->getUriBuilder()
                ->reset()
                ->setTargetPageUid($this->settings['redirectPid'])
                ->buildFrontendUri();

            $this->redirectToURI($redirectUrl);
        } else {
            $arguments = [
                'success' => true,
            ];

            $this->redirect('display', 'Mailchimp', 'rzmailchimp', $arguments);
        }
    }

}
# rzmailchimp

You have to add the `rzmailchimp` parameter in `realurl` configuration:

```php
'cache' => [
  'banUrlsRegExp' => '/tx_solr|tx_rzmailchimp_mailchimp|tx_indexed_search|(?:^|\?|&)q=/'
],
```
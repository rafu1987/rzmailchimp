#
# Table structure for table 'tx_rzmailchimp_domain_model_field'
#
CREATE TABLE tx_rzmailchimp_domain_model_field (
	title varchar(255) DEFAULT '' NOT NULL,
	value varchar(255) DEFAULT '' NOT NULL,
	type text NOT NULL,
	options text NOT NULL,
	required tinyint(4) unsigned DEFAULT '0' NOT NULL,
	multiple tinyint(4) unsigned DEFAULT '0' NOT NULL,
);

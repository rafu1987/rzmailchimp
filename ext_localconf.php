<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'RZ.' . $extKey,
            'Mailchimp',
            [
                'Mailchimp' => 'display, subscribe',
            ],
            // non-cacheable actions
            [
                'Mailchimp' => '',
            ]
        );

        if (TYPO3_MODE === 'BE') {
            $icons = [
                'ext-rzmailchimp-wizard-icon' => 'ce_wiz.svg',
            ];

            $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
            foreach ($icons as $identifier => $path) {
                $iconRegistry->registerIcon(
                    $identifier,
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:rzmailchimp/Resources/Public/Icons/' . $path]
                );
            }
        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:rzmailchimp/Configuration/TSconfig/ContentElementWizard.txt">');

        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\Dispatcher');
        $signalSlotDispatcher->connect(
            'In2code\Powermail\Controller\FormController',
            'createActionAfterSubmitView',
            'RZ\Rzmailchimp\Controller\PowermailController',
            'mailchimp',
            false
        );

    },
    $_EXTKEY
);
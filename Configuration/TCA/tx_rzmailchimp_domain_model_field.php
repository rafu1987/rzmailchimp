<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

defined('TYPO3_MODE') || die('Access denied.');

return [
    'ctrl' => [
        'title' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'hideTable' => true,
        'versioningWS' => false,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,value,options,',
        'iconfile' => 'EXT:rzmailchimp/Resources/Public/Icons/tx_rzmailchimp_domain_model_field.svg',
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, title, value, options, multiple, required',
    ],
    'types' => [
        '1' => [
            'showitem' => 'type, title, value, options, multiple, required, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, sys_language_uid, l10n_parent, l10n_diffsource, hidden, starttime, endtime',
        ],
    ],
    'palettes' => [
        '1' => [
            'showitem' => '',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple',
                    ],
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0,
                    ],
                ],
                'foreign_table' => 'tx_rzmailchimp_domain_model_field',
                'foreign_table_where' => 'AND tx_rzmailchimp_domain_model_field.pid=###CURRENT_PID### AND tx_rzmailchimp_domain_model_field.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],

        'type' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.type',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.type.1',
                        'Input',
                    ],
                    [
                        'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.type.2',
                        'Email',
                    ],
                    [
                        'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.type.3',
                        'Select',
                    ],
                ],
                'size' => 1,
                'maxitems' => 1,
            ],
        ],

        'title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required',
            ],
        ],

        'value' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.value',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required',
            ],
        ],

        'options' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:type:=:Select',
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.options',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 5,
                'eval' => 'trim, required',
            ],
        ],

        'multiple' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:type:=:Select',
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.multiple',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],

        'required' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:rzmailchimp/Resources/Private/Language/locallang_db.xlf:tx_rzmailchimp_domain_model_field.required',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
            ],
        ],

    ],
];
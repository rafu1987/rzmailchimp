plugin.tx_rzmailchimp {
  view {
    # cat=plugin.tx_rzmailchimp/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:rzmailchimp/Resources/Private/Templates/
    # cat=plugin.tx_rzmailchimp/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:rzmailchimp/Resources/Private/Partials/
    # cat=plugin.tx_rzmailchimp/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:rzmailchimp/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_rzmailchimp//a; type=string; label=Default storage PID
    storagePid =
  }
  settings {
    # cat=plugin.tx_rzmailchimp//a; type=boolean; label=Add Parsley?
    addParsley = 1
  }
}
plugin.tx_rzmailchimp {
  view {
    templateRootPaths {
      0 = {$plugin.tx_rzmailchimp.view.templateRootPath}
    }
    partialRootPaths {
      0 = {$plugin.tx_rzmailchimp.view.partialRootPath}
    }
    layoutRootPaths {
      0 = {$plugin.tx_rzmailchimp.view.layoutRootPath}
    }
  }
  persistence {
    storagePid = {$plugin.tx_rzmailchimp.persistence.storagePid}
  }
  features {
    # uncomment the following line to enable the new Property Mapper.
    # rewrittenPropertyMapper = 1
  }
  settings {
    addParsley = {$plugin.tx_rzmailchimp.settings.addParsley}
  }
}
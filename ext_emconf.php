<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "rzmailchimp".
 *
 * Auto generated 16-10-2014 15:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Mailchimp',
    'description' => 'Mailchimp subscribe extension',
    'category' => 'plugin',
    'version' => '9.5.0',
    'state' => 'beta',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => false,
    'author' => 'Raphael Zschorsch',
    'author_email' => 'rafu1987@gmail.com',
    'author_company' => null,
    'constraints' => [
        'depends' => [
            'extbase' => '9.5.0-9.5.99',
            'fluid' => '9.5.0-9.5.99',
            'typo3' => '9.5.0-9.5.99',
            'php' => '5.6.0-7.3.99',
            'vhs' => '',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'RZ\\Rzmailchimp\\' => 'Classes',
        ],
    ],
];